package com.test.project.service;

import com.test.project.model.Excel;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;

@Service
public class ExportFile {

    /**
     * Read xlsx file to get information to put on new xlsx
     */

    public void export() throws IOException {
        Resource resource = new ClassPathResource("Mapeo Colaborativo datos.gob.mx __ bit.ly_rescateMX.XLSX");
        File file = resource.getFile();
        Workbook wb = WorkbookFactory.create(file);
        CellStyle origStyle = wb.getCellStyleAt(1);
        Sheet sheetBack = wb.getSheetAt(1);
        ArrayList<Excel> listaExcel = new ArrayList<Excel>();

        for (Row row : sheetBack) {
            Excel excel = new Excel();
            excel.setCentro(row.getCell(1).toString());
            excel.setId(row.getCell(0).toString());
            excel.setNecesidad(row.getCell(3).toString());
            listaExcel.add(excel);
        }

        SXSSFWorkbook newWB = new SXSSFWorkbook();
        SXSSFSheet sheet = newWB.createSheet("Necesidades");
        CellStyle newStyle = newWB.createCellStyle();
        newStyle.cloneStyleFrom(origStyle);

        SXSSFRow row;


        int cont = 0;
        SXSSFCell cell;
        for (Excel e : listaExcel) {
            if (!listaExcel.isEmpty()) {
                row = sheet.createRow(cont);
                cell = row.createCell(0);
                cell.setCellStyle(newStyle); // se añade el style crea anteriormente
                cell.setCellValue(e.getId().replace(".0", "")); //se añade el contenido

                cell = row.createCell(1);
                cell.setCellStyle(newStyle); // se añade el style crea anteriormente
                cell.setCellValue(e.getCentro()); //se añade el contenido

                cell = row.createCell(2);
                cell.setCellStyle(newStyle); // se añade el style crea anteriormente
                cell.setCellValue(e.getNecesidad()); //se añade el contenido
                sheet.flushRows();
                cont++;
            }
        }
        newWB.write(new FileOutputStream("Centros de Acopio – Necesidades.xlsx"));
    }
}
