package com.test.project.controller;

import com.test.project.service.ExportFile;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class MainController {

    @Autowired
    private ExportFile exportFile;

    //Method to create file with new information
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void export() throws IOException, InvalidFormatException {
        exportFile.export();
    }
}
